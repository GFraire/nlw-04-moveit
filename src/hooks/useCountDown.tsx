import { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { useChallenges } from "./useChallenges";

interface CountDownContextData {
  minutes: number;
  seconds: number;
  hasFinished: boolean;
  isActive: boolean;
  startCountDown: () => void;
  resetCountDown: () => void;
}

interface CountDownProps {
  children: ReactNode;
}

let countDownTimeout: NodeJS.Timeout

const CountDownContext = createContext({} as CountDownContextData);

export function CountDownProvider({ children }: CountDownProps) {
  const [time, setTime] = useState(25 * 60);
  const [isActive, setIsActive] = useState(false);
  const [hasFinished, setHasFinished] = useState(false);

  const { startNewChallenge } = useChallenges();

  const minutes = Math.floor(time / 60);
  const seconds = time % 60;

  function startCountDown() {
    setIsActive(true)
  }

  function resetCountDown() {
    clearTimeout(countDownTimeout);
    setIsActive(false);
    setTime(25 * 60);
    setHasFinished(false);
  }

  useEffect(() => {
    if(isActive && time > 0) {
      countDownTimeout = setTimeout(() => {
        setTime(time - 1)
      }, 1000)
    } else if(isActive && time === 0) {
      setHasFinished(true);
      setIsActive(false);
      startNewChallenge();
    }
  }, [isActive, time])

  return (
    <CountDownContext.Provider value={{ hasFinished, isActive, minutes, resetCountDown, seconds, startCountDown}}>
      {children}
    </CountDownContext.Provider>
  )
}

export function useCountDown() {
  const context = useContext(CountDownContext);

  return context
}