import { useCountDown } from "../hooks/useCountDown";
import styles from "../styles/components/CountDown.module.scss"

export function CountDown() {
  const { minutes, seconds, hasFinished, isActive, startCountDown, resetCountDown } = useCountDown();

  const [minuteLeft, minuteRight] = String(minutes).padStart(2, "0").split("");
  const [secondLeft, secondRight] = String(seconds).padStart(2, "0").split("");

  return (
    <div>
      <div className={styles.countDownContainer}>
        <div>
          <span>{minuteLeft}</span>
          <span>{minuteRight}</span>
        </div>
        <span>:</span>
        <div>
          <span>{secondLeft}</span>
          <span>{secondRight}</span>
        </div>
      </div>

      { hasFinished ? (
        <button
          disabled
          className={styles.countDownButton}
        >
          Ciclo encerrado
          <img src="icons/check.svg" alt="check" />
        </button>
      ) : (
        <>
          { !isActive ? (
            <button
              className={styles.countDownButton}
              type="button"
              onClick={startCountDown}
            >
              Iniciar ciclo
              <img src="icons/play.svg" alt="play" />
            </button>
          ) : (
            <button
              className={`${styles.countDownButton} ${styles.countDownButtonActive}`}
              type="button"
              onClick={resetCountDown}
            >
              Abandonar ciclo
              <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#666666"/>
              </svg>
            </button>
          ) }
        </>
      )}
    </div>
  )
}