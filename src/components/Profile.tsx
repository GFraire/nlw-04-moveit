import { useChallenges } from "../hooks/useChallenges"
import styles from "../styles/components/Profile.module.scss"

export function Profile() {
  const { level } = useChallenges();
  return (
    <div className={styles.profileContainer}>
      <img src="https://github.com/gfraire.png" alt="gfraire" />

      <div>
        <strong>Gabriel Freire</strong>
        <p>
          <img src="icons/level.svg" alt="Level" />
          Level {level}
        </p>
      </div>
    </div>
  )
}