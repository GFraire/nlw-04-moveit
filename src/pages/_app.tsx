import { ChallengesProvider } from "../hooks/useChallenges"
import "../styles/styles.css"

function MyApp({ Component, pageProps }) {
  return (
    <Component {...pageProps} />
  )
}

export default MyApp
